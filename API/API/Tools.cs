﻿namespace API
{
    public static class Tools
    {
        public static string Convert(this EnumStatus status)
        {
            return status switch
            {
                EnumStatus.UNKNOWN => "UNKNOWN",
                EnumStatus.WAITING => "WAITING",
                EnumStatus.STARTED => "STARTED",
                EnumStatus.FINISHED => "FINISHED",
                EnumStatus.ERROR => "ERROR",
                _ => "ERROR"
            };
        }
    }
}
