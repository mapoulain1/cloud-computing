﻿namespace API
{
    public enum EnumStatus
    {
        UNKNOWN,
        WAITING,
        STARTED,
        FINISHED,
        ERROR
    }
}
