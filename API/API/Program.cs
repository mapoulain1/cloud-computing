using System.Diagnostics;
using API;
using SwiftClient;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

var swiftClient = new Client().WithCredentials(new SwiftCredentials
{

    Username = "mapoulain1",
    Password = "azerty",
    Endpoints = new List<string> { "http://10.20.22.70:8080/v1/AUTH_a1ff7cef450045e1a72d6a0ad6dd73f1/" }
}).SetRetryCount(10).SetRetryPerEndpointCount(2).SetLogger(new SwiftConsoleLogger());

app.UseSwagger();
app.UseSwaggerUI();
app.UseHttpsRedirection();

app.MapGet("/ping", () => "pong").WithName("ping").WithOpenApi();

var latestId = 0;
var jobs = new List<Job>();

app.MapGet("/jobs/", () => Results.Ok(jobs));

app.MapGet("/job/{id}/status", (int id) =>
{
    Job? job = jobs.FirstOrDefault(j => j.Id == id);
    if (job is null) return Results.NotFound();
    return Results.Ok(job);
});

app.MapGet("/job/{id}/image", (int id) =>
{
    Job? job = jobs.FirstOrDefault(j => j.Id == id);
    if (job is null) return Results.NotFound();
    var filename = $"{Environment.CurrentDirectory}/povray/glsbng{job.Frame:00}.png";
    if (!File.Exists(filename)) return Results.NotFound();
    return Results.File(filename, "image/png");
});

app.MapPut("/job/{id}/start", (int id) =>
{
    Job? job = jobs.FirstOrDefault(j => j.Id == id);
    if (job is null) return Results.NotFound();
    try
    {
        job.process.Start();
        job.status = EnumStatus.STARTED;
    }
    catch (Exception e)
    {
        Console.WriteLine(e);
        job.status = EnumStatus.ERROR;
    }
    return Results.Ok(job);

});

app.MapPost("/job", (int frame) =>
{
    var job = new Job(latestId++, frame);
    job.process = new Process();
    job.process.StartInfo.FileName = $"{Environment.CurrentDirectory}/povray/povray";
    job.process.StartInfo.WorkingDirectory += "povray";
    job.process.StartInfo.Arguments = $"+A +W50 +H50 +Lshare/povray-3.6/include/ +SF{job.Frame} +EF{job.Frame} glsbng.ini";
    job.process.EnableRaisingEvents = true;
    job.process.Exited += (_, _) =>
    {
        job.status = EnumStatus.FINISHED;

        swiftClient.PutObjectAsync("Povray", "test",
            File.ReadAllBytes($"{Environment.CurrentDirectory}/povray/glsbng{job.Frame:00}.png"),
            new Dictionary<string, string> { { "Content-Type", "image/png" } });
    };
    job.process.ErrorDataReceived += (_, _) => job.status = EnumStatus.ERROR;

    jobs.Add(job);
    return Results.Ok(job);
});

app.MapPost("/video", () =>
{
    var process = new Process();

    process.StartInfo.FileName = "convert";
    process.StartInfo.WorkingDirectory += "povray";
    process.StartInfo.Arguments = "glsbng*.png -delay 16 -quality 100 glsbng.gif";

    process.Start();

    return Results.Ok();
});

app.MapGet("/video", () =>
{
    var filename = $"{Environment.CurrentDirectory}/povray/glsbng.gif";
    if (!File.Exists(filename)) return Results.NotFound();

    swiftClient.PutObjectAsync("Povray", "testVideo",
        File.ReadAllBytes($"{Environment.CurrentDirectory}/povray/glsbng.gif"),
        new Dictionary<string, string> { { "Content-Type", "image/gif" } });

    return Results.File(filename, "iamge/gif");
});

app.Run();
